CC=clang++

LIBS = `pkg-config --libs sfml-all`
#LIBS = -framework sfml-window -framework sfml-graphics -framework sfml-system

# Build configurations
CCFLAGS_DEBUG=-O0 -DDEBUG -Wall -pedantic -g
CCFLAGS_RELEASE=-Ofast

# Default flags for compiling
CCFLAGS = -std=c++1y
CCFLAGS += $(CCFLAGS_DEBUG)

Hallways: main.o
	$(CC) $(LIBS) main.o -o Hallways

main.o: main.cc math.hh palette.h
	$(CC) $(CCFLAGS) -c main.cc -o main.o

clean:
	rm Hallways *.o

run: Hallways
	bash -c "./Hallways"
